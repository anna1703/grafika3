import {Point3D} from "./point3-d";
import {Sphere} from "./sphere";
import {Color} from "./color";

export class Constants {
  static HEIGHT: number = 500;
  static WIDTH: number = 500;
  static LEFT_BOTTOM_POINT: Point3D = new Point3D(-1, -1, 1);
  static RIGHT_TOP_POINT: Point3D = new Point3D(1, 1, 1);
  static OBSERVER: Point3D = new Point3D(0, 0, 0);
  static LIGHT_SOURCE: Point3D = new Point3D(-3, 3, -5);
  static SPHERE: Sphere = new Sphere(new Point3D(0, 0, 3), 1.5);
  static BACKGROUND_COLOR: Color = new Color(255, 255, 255);
}
