import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PhongParamsFormComponent } from './phong-params-form.component';

describe('PhongParamsFormComponent', () => {
  let component: PhongParamsFormComponent;
  let fixture: ComponentFixture<PhongParamsFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PhongParamsFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PhongParamsFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
