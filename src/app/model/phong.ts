export class Phong {
  ia: number; // natezenie swiatla tla
  ip: number; // natezenie swiatla punktowego zrodla
  ka: number; // wspolczynnik odbicia swiatla tla
  kd: number; // wspolczynnik odbicia rozproszonego
  ks: number; // wspolczynnik odbicia kierunkowego
  c: number;  // stala tlumienia
  n: number;  // wykladnik potegi

  constructor(ia: number, ip: number, ka: number, kd: number, ks: number, c: number, n: number) {
    this.ia = ia;
    this.ip = ip;
    this.ka = ka;
    this.kd = kd;
    this.ks = ks;
    this.c = c;
    this.n = n;
  }

}
