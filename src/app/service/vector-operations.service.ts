import { Injectable } from '@angular/core';
import {Vector3D} from "../model/vector3-d";

@Injectable({
  providedIn: 'root'
})
export class VectorOperationsService {

  constructor() { }

  public static dotProduct(vector1: Vector3D, vector2: Vector3D): number {
    return vector1.x * vector2.x + vector1.y * vector2.y + vector1.z * vector2.z;
  }

  public static norm(vector: Vector3D): number {
    return Math.sqrt(VectorOperationsService.dotProduct(vector, vector));
  }

  public static normalize(vector: Vector3D): Vector3D {
    let norm: number = VectorOperationsService.norm(vector);
    let newVector: Vector3D = new Vector3D();
    newVector.initWithValues(vector.x / norm, vector.y / norm, vector.z / norm);
    return newVector;
  }

  public static multiply(vector: Vector3D, d: number): Vector3D {
    let newVector: Vector3D = new Vector3D();
    newVector.initWithValues(vector.x * d, vector.y * d, vector.z * d);
    return newVector;
  }

  public static substract(vector1: Vector3D, vector2: Vector3D): Vector3D {
    let newVector: Vector3D = new Vector3D();
    newVector.initWithValues(vector1.x - vector2.x, vector1.y - vector2.y, vector1.z - vector2.z);
    return newVector;
  }

}
