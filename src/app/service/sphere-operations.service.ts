import { Injectable } from '@angular/core';
import {Point3D} from "../model/point3-d";
import {Sphere} from "../model/sphere";

@Injectable({
  providedIn: 'root'
})
export class SphereOperationsService {

  constructor() { }

  public static getCrossingPoint(sphere: Sphere, point1: Point3D, point2: Point3D): Point3D {
    // point1 - obserwator
    let dx: number = point2.x - point1.x;
    let dy: number = point2.y - point1.y;
    let dz: number = point2.z - point1.z;

    let a: number = Math.pow(dx, 2) + Math.pow(dy, 2) + Math.pow(dz, 2);
    let b: number = 2 * (dx * (point1.x - sphere.centerPoint.x) + dy * (point1.y - sphere.centerPoint.y) + dz * (point1.z - sphere.centerPoint.z));
    let c: number = Math.pow(point1.x - sphere.centerPoint.x, 2) + Math.pow(point1.y - sphere.centerPoint.y, 2) + Math.pow(point1.z - sphere.centerPoint.z, 2) - Math.pow(sphere.radius, 2);
    let delta: number = b * b - 4 * a * c;

    if (delta < 0) {
      return null;
    } else if (delta === 0) {
      let t: number = -b / 2 * a;
      let x: number = point1.x + t * (point2.x - point1.x);
      let y: number = point1.y + t * (point2.y - point1.y);
      let z: number = point1.z + t * (point2.z - point1.z);
      return new Point3D(x, y, z);
    } else {
      let t1: number = (-b - Math.sqrt(delta)) / (2 * a);
      let t2: number = (-b + Math.sqrt(delta)) / (2 * a);
      let t: number = Math.min(t1, t2);

      let x: number = point1.x + t * (point2.x - point1.x);
      let y: number = point1.y + t * (point2.y - point1.y);
      let z: number = point1.z + t * (point2.z - point1.z);
      return new Point3D(x, y, z);
    }
  }

}
