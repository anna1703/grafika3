import { Injectable } from '@angular/core';
import {Point3D} from "../model/point3-d";
import {Sphere} from "../model/sphere";
import {PixelsScreen} from "../model/pixels-screen";
import {Phong} from "../model/phong";
import {Vector3D} from "../model/vector3-d";
import {VectorOperationsService} from "./vector-operations.service";
import {Color} from "../model/color";
import {Constants} from "../model/constants";
import {Mesh} from "../model/mesh";
import {SphereOperationsService} from "./sphere-operations.service";

@Injectable({
  providedIn: 'root'
})
export class PhongOperationsService {

  constructor() { }

  public static getSpherePointLight(phongParams: Phong, lightSource: Point3D, observer: Point3D, sphere: Sphere, pointOnSphere: Point3D): number {
    if (pointOnSphere == null) {
      return null;
    }

    let N: Vector3D = new Vector3D(); // wektor normalny
    N.initWithValuesPoints(sphere.centerPoint, pointOnSphere);
    N = VectorOperationsService.normalize(N);

    let V: Vector3D = new Vector3D(); // wektor do obserwatora
    V.initWithValuesPoints(pointOnSphere, observer);
    V = VectorOperationsService.normalize(V);

    let L: Vector3D = new Vector3D(); // wektor do zrodla swiatla
    L.initWithValuesPoints(pointOnSphere, lightSource);
    let dist: number = VectorOperationsService.norm(L); // odleglosc punktu sfery od zrodla swiatla
    L = VectorOperationsService.normalize(L);

    let R: Vector3D = VectorOperationsService.substract(VectorOperationsService.multiply(VectorOperationsService.multiply(N, 2), VectorOperationsService.dotProduct(N, L)), L); // wektor odbity
    R = VectorOperationsService.normalize(R);

    return phongParams.ia * phongParams.ka + phongParams.ip * (phongParams.kd * Math.max(VectorOperationsService.dotProduct(N, L), 0) + phongParams.ks * Math.pow(Math.max(VectorOperationsService.dotProduct(R, V), 0), phongParams.n)) / (phongParams.c + dist);
  }

  public static getLightScreen(phongParams: Phong): PixelsScreen {
    let mesh: Mesh = new Mesh(Constants.LEFT_BOTTOM_POINT, Constants.RIGHT_TOP_POINT);
    let observer: Point3D = Constants.OBSERVER;
    let lightSource: Point3D = Constants.LIGHT_SOURCE;
    let sphere: Sphere = Constants.SPHERE;

    let screen: PixelsScreen  = new PixelsScreen();

    let maxI: number = 0.0;
    let minI: number = 10000.0;
    let lightBuffor: Array<Array<number>> = [];

    for (let y: number = 0; y < Constants.HEIGHT; y++) {
      lightBuffor[y] = [];
      for (let x: number = 0; x < Constants.WIDTH; x++) {
        let current: Point3D = mesh.points[y][x];
        let crossingPoint: Point3D = SphereOperationsService.getCrossingPoint(sphere, observer, current);
        let I: number = PhongOperationsService.getSpherePointLight(phongParams, lightSource, observer, sphere, crossingPoint);
        if (I != null) {
          if (I > maxI) {
            maxI = I;
          }
          if (I < minI) {
            minI = I;
          }
        }
        lightBuffor[y][x] = I;
      }
    }

    for (let y: number = 0; y < Constants.HEIGHT; y++) {
      for (let x: number = 0; x < Constants.WIDTH; x++) {
        let I: number = lightBuffor[y][x];
        if (I != null) {
          let v: number = Math.ceil(255*I / (maxI + 0.001));
          screen.setPixelColor(x, y, new Color(v, v, v));
        }
      }
    }

    return screen;
  }
}
