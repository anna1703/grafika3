import {Point3D} from "./point3-d";

export class Sphere {
  centerPoint: Point3D;
  radius: number;

  constructor(centerPoint: Point3D, radius: number) {
    this.centerPoint = centerPoint;
    this.radius = radius;
  }

}
