import {Component} from '@angular/core';
import {Phong} from "./model/phong";
import {PhongOperationsService} from "./service/phong-operations.service";
import {PixelsScreen} from "./model/pixels-screen";

@Component({
  selector: 'gk-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  screen: PixelsScreen;

  changeParams(event: Phong): void {
    this.screen = PhongOperationsService.getLightScreen(event);
  }
}
