import {Component, OnInit, ViewChild, ElementRef, Input, OnChanges, SimpleChanges} from '@angular/core';
import {Constants} from "../model/constants";
import {PixelsScreen} from "../model/pixels-screen";
import {Color} from "../model/color";

@Component({
  selector: 'gk-canvas',
  templateUrl: './canvas.component.html',
  styleUrls: ['./canvas.component.scss']
})
export class CanvasComponent implements OnInit, OnChanges {

  @ViewChild('canvas')
  canvasRef: ElementRef;

  @Input()
  screen: PixelsScreen;

  canvas: any;
  ctx: any;

  constructor() { }

  ngOnInit() {
    this.canvas = this.canvasRef['nativeElement'];
    this.ctx = this.canvas.getContext('2d');

    this.canvas.width = Constants.WIDTH;
    this.canvas.height = Constants.HEIGHT;

    let transX: number = 0;
    let transY: number = this.canvas.height;
    this.ctx.translate(transX, transY);
    this.ctx.scale(1, -1);
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes['screen'] && changes['screen'].currentValue && this.screen && this.screen.pixels) {
      this.clear();
      this.print();
    }
  }

  print(): void {
    for (let y: number = 0; y < Constants.HEIGHT; y++) {
      for (let x: number = 0; x < Constants.WIDTH; x++) {
        let pixel: Color = this.screen.pixels[y][x];
        this.ctx.fillStyle = "rgb("+pixel.r+","+pixel.g+","+pixel.b+")";
        this.ctx.fillRect(x, y, 1, 1);
      }
    }
  }

  clear(): void {
    // Store the current transformation matrix
    this.ctx.save();
    // Use the identity matrix while clearing the canvas
    this.ctx.setTransform(1, 0, 0, 1, 0, 0);
    this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
    // Restore the transform
    this.ctx.restore();
  }
}

