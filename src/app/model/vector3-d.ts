import {Point3D} from "./point3-d";

export class Vector3D {
  x: number;
  y: number;
  z: number;

  constructor() {
    this.x = 0;
    this.y = 0;
    this.z = 0;
  }

  initWithValues(x: number, y: number, z: number): void {
    this.x = x;
    this.y = y;
    this.z = z;
  }

  initWithValuesPoints(startPoint: Point3D, endPoint: Point3D): void {
    this.x = endPoint.x - startPoint.x;
    this.y = endPoint.y - startPoint.y;
    this.z = endPoint.z - startPoint.z;
  }

}
