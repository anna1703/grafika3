import {Color} from "./color";
import {Constants} from "./constants";

export class PixelsScreen {
  pixels: Array<Array<Color>>;

  constructor() {
    this.pixels = [];
    for (let y: number = 0; y < Constants.HEIGHT; y++) {
      this.pixels[y] = [];
      for (let x: number = 0; x < Constants.WIDTH; x++) {
        this.pixels[y][x] = Constants.BACKGROUND_COLOR;
      }
    }
  }

  public setPixelColor(x: number, y: number, color: Color): void {
    this.pixels[y][x] = color;
  }
}
