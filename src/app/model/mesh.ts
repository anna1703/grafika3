import {Point3D} from "./point3-d";
import {Constants} from "./constants";

export class Mesh {
  leftBottomPoint: Point3D;
  rightTopPoint: Point3D;

  points: Array<Array<Point3D>>;

  constructor(leftBottomPoint: Point3D, rightTopPoint: Point3D) {
    this.leftBottomPoint = leftBottomPoint;
    this.rightTopPoint = rightTopPoint;
    this.createPointsMesh();
  }

  private createPointsMesh(): void {
    this.points = [];

    let dx: number = (this.rightTopPoint.x - this.leftBottomPoint.x) / Constants.WIDTH;
    let dy: number = (this.rightTopPoint.y - this.leftBottomPoint.y) / Constants.HEIGHT;
    // wsp z musza byc takie same!

    for (let y: number = 0; y < Constants.HEIGHT; y++) {
      this.points[y] = [];
      for (let x: number = 0; x < Constants.WIDTH; x++) {
        this.points[y][x] = new Point3D(this.leftBottomPoint.x + x * dx, this.leftBottomPoint.y + y * dy, this.leftBottomPoint.z);
      }
    }
  }
}
