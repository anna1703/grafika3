import {Component, OnInit, Output, EventEmitter} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Phong} from "../model/phong";

@Component({
  selector: 'gk-phong-params-form',
  templateUrl: './phong-params-form.component.html',
  styleUrls: ['./phong-params-form.component.scss']
})
export class PhongParamsFormComponent implements OnInit {
  phongForm: FormGroup;

  @Output()
  phongParams: EventEmitter<Phong> = new EventEmitter<Phong>();

  constructor(private fb: FormBuilder) {  }

  ngOnInit() {
    this.phongForm = this.fb.group({
      ia: [1, Validators.required],
      ip: [1, Validators.required],
      ka: [0.05, Validators.required],
      kd: [1, Validators.required],
      ks: [0, Validators.required],
      c: [2, Validators.required],
      n: [1, Validators.required],
    });
  }

  saveParams(): void {
    this.phongParams.next(this.phongForm.value);
  }

}
