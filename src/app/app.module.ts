import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {AngularMaterialModule} from "./angular-material/angular-material.module";
import 'hammerjs';
import {ReactiveFormsModule, FormsModule} from "@angular/forms";

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CanvasComponent } from './canvas/canvas.component';
import { PhongParamsFormComponent } from './phong-params-form/phong-params-form.component';

@NgModule({
  declarations: [
    AppComponent,
    CanvasComponent,
    PhongParamsFormComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AngularMaterialModule,
    ReactiveFormsModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
